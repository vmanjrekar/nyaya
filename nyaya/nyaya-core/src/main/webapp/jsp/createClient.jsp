<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="in.co.vineet.antahkaran.nyaya.data.ClientDao" %>
<%@ page import="in.co.vineet.antahkaran.nyaya.entity.ClientEntity" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html>

<%@page import="java.util.List"%>

<html>

<script>

function verify(){
    if(!document.getElementById('clientName').value.trim().length){
        alert("Please enter the Client Name");
        return false;
    }

    if(!document.getElementById('caseNumber').value.trim().length){
            alert("Please enter the Client Case Number");
            return false;
        }
 }

</script>


  <head>
    <title>Clients List</title>
    <link rel="stylesheet" type="text/css" href="css/main.css"/>
      <meta charset="utf-8">
  </head>
  <body>

  <div class="main">

  <div class="headline">New Client</div>

  <form action="/new" onsubmit= "return verify();" method="post" accept-charset="utf-8">
    <table>
      <tr>
        <td><label for="clientName">Name of the Client:</label></td>
        <td><input type="text" name="clientName" id="clientName" size="50"/></td>
      </tr>
      <tr>
        <td valign="caseNumber"><label for="caseNumber">Client Case Number: </label></td>
        <td><input type="text" name="caseNumber" id="caseNumber"/></td>
      </tr>
    <tr>
      <td valign="top"><label for="mobileNumber">Mobile Number</label></td>
      <td><input type="text" name="mobileNumber" id="mobileNumber" size="10" /></td>
    </tr>
    <tr>
        <td colspan="2" align="right"><input type="submit" value="Create" /></td>
      </tr>
    </table>
  </form>

  </div>



<%
ClientDao clientDao = new ClientDao();


List<ClientEntity> clientEntities = new ArrayList<ClientEntity>();

clientEntities = clientDao.listClients();

%>

<div style="clear: both;"/>
You have a total number of <%= clientEntities.size() %>  clients.

<table>
  <tr>
      <th>Client Name </th>
      <th>Case Number</th>
      <th>Mobile Number</th>
      <th>Judgement Date</th>
      <th>Created Date</th>
    </tr>

<% for (ClientEntity entity : clientEntities) {%>
<tr>
<td>
<%=entity.getClientName()%>
</td>
<td>
<%=entity.getCaseNumber()%>
</td>
<td>
<%=entity.getMobileNumber()%>
</td>
<td>
    <%=entity.getJudgementDate()==null?"":entity.getJudgementDate()%>
</td>
    <td>
        <%=entity.getCreatedDate()==null?"":entity.getCreatedDate()%>
    </td>
</tr>
<%}
%>
</table>


<hr />


</body>
</html>