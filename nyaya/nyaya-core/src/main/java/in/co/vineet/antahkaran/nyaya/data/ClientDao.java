package in.co.vineet.antahkaran.nyaya.data;

import com.google.appengine.api.datastore.*;
import com.google.appengine.repackaged.org.joda.time.DateTime;
import in.co.vineet.antahkaran.nyaya.entity.ClientEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientDao {

    DatastoreService dataStore = DatastoreServiceFactory.getDatastoreService();

    public List<ClientEntity> listClients() {
        List<ClientEntity> clientEntities = new ArrayList<ClientEntity>();
        Query query = new Query("ClientEntity").addSort("clientName");
        List<Entity> entities = dataStore.prepare(query).asList(FetchOptions.Builder.withDefaults());
        for (Entity entity : entities) {
            ClientEntity clientEntity = new ClientEntity(
                    entity.getProperty("clientName").toString(),
                    entity.getProperty("caseNumber").toString(),
                    entity.getProperty("cellNumber").toString(),
                    (Date) entity.getProperty("judgementDate"),
                    (Date) entity.getProperty("createdDate"));
            clientEntities.add(clientEntity);
        }
        return clientEntities;
    }

    public void add(String clientName, String caseNumber, String cellNumber) {
        Entity entity = new Entity(KeyFactory.createKey("ClientEntity", clientName + caseNumber));
        entity.setProperty("clientName", clientName);
        entity.setProperty("cellNumber", cellNumber);
        entity.setProperty("caseNumber", caseNumber);
        entity.setProperty("createdDate", new Date());
        DatastoreService datastore =
                DatastoreServiceFactory.getDatastoreService();
        datastore.put(entity);
    }

    public List<ClientEntity> getClient(String key) {

        List<ClientEntity> clientEntities = new ArrayList<ClientEntity>();

        Key todoKey = KeyFactory.createKey("ClientEntity", key);
        Entity entity;
        try {

            entity = dataStore.get(todoKey);
            String cellNumber = (String) entity.getProperty("cellNumber");
            String caseNumber = (String) entity.getProperty("caseNumber");
            String clientName = (String) entity.getProperty("clientName");
            Date judgementDate = (Date) entity.getProperty("judgementDate");
            Date createdDate = (Date) entity.getProperty("createdDate");
            clientEntities.add(new ClientEntity(clientName, caseNumber, cellNumber, judgementDate, createdDate));

        } catch (EntityNotFoundException e) {
        }
        return clientEntities;
    }

    public void update(ClientEntity clientEntity) {
        Entity entity = new Entity(KeyFactory.createKey("ClientEntity", clientEntity.getClientName() + clientEntity.getCaseNumber()));
        entity.setProperty("clientName", clientEntity.getClientName());
        entity.setProperty("cellNumber", clientEntity.getMobileNumber());
        entity.setProperty("caseNumber", clientEntity.getCaseNumber());
        entity.setProperty("judgementDate", new DateTime(clientEntity.getJudgementDate()).toDate());
        entity.setProperty("createdDate", new Date());
        DatastoreService datastoreService =
                DatastoreServiceFactory.getDatastoreService();
        datastoreService.put(entity);
    }

    public List<ClientEntity> getBy(String clientName) {
        List<ClientEntity> clientEntities = new ArrayList<ClientEntity>();
        Query.Filter nameFilter =
                new Query.FilterPredicate("clientName",Query.FilterOperator.EQUAL, clientName);
        Query query = new Query("ClientEntity").setFilter(nameFilter).addSort("createdDate");
        List<Entity> entities = dataStore.prepare(query).asList(FetchOptions.Builder.withDefaults());
        for (Entity entity : entities) {
            ClientEntity clientEntity = new ClientEntity(
                    entity.getProperty("clientName").toString(),
                    entity.getProperty("caseNumber").toString(),
                    entity.getProperty("cellNumber").toString(),
                    (Date) entity.getProperty("judgementDate"),
                    (Date) entity.getProperty("createdDate"));
            clientEntities.add(clientEntity);
        }
        return clientEntities;
    }
}
