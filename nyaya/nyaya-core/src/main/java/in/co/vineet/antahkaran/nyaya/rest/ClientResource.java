package in.co.vineet.antahkaran.nyaya.rest;

import in.co.vineet.antahkaran.nyaya.entity.ClientEntity;
import in.co.vineet.antahkaran.nyaya.service.ClientService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("client")
public class ClientResource {

    ClientService clientService = new ClientService();

    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {

        return Response.ok().entity(clientService.getAll()).build() ;
    }

    @Path("name/{clientName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByName(@PathParam("clientName") String clientName) {

        return Response.ok().entity(clientService.getBy(clientName)).build() ;
    }


    @Path("add")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String createClient(ClientEntity clientEntity) {

        clientService.createClient(clientEntity);
        return "SUCCESS";
    }

    @Path("update")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public String updateClient(ClientEntity clientEntity) {

        clientService.updateClient(clientEntity);
        return "SUCCESS";
    }

}
