package in.co.vineet.antahkaran.nyaya.service;

import in.co.vineet.antahkaran.nyaya.data.ClientDao;
import in.co.vineet.antahkaran.nyaya.entity.ClientEntity;

import java.util.List;

public class ClientService {

    ClientDao clientDao = new ClientDao();

    public List<ClientEntity> getAll() {
        return clientDao.listClients();
    }

    public void createClient(ClientEntity clientEntity) {
        clientDao.add(clientEntity.getClientName(),clientEntity.getCaseNumber(),
                      clientEntity.getMobileNumber());
    }

    public void updateClient(ClientEntity clientEntity) {
        clientDao.update(clientEntity);
    }

    public List<ClientEntity> getBy(String clientName) {
       return clientDao.getBy(clientName);
    }
}
