package in.co.vineet.antahkaran.nyaya.servlet;

import in.co.vineet.antahkaran.nyaya.data.ClientDao;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientServlet extends HttpServlet {
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        System.out.println("Creating new todo ");

        String clientName = checkNull(req.getParameter("clientName"));
        String caseNumber = checkNull(req.getParameter("caseNumber"));
        String mobileNumber = checkNull(req.getParameter("mobileNumber"));

        new ClientDao().add(clientName, caseNumber, mobileNumber);

        resp.sendRedirect("/jsp/createClient.jsp");
    }

    private String checkNull(String string) {
        if (string == null) {
            return "";
        }
        return string;
    }
}
