package in.co.vineet.antahkaran.nyaya.entity;

import com.google.appengine.repackaged.org.joda.time.LocalDate;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import java.util.Date;

@XmlRootElement(name = "client")
public class ClientEntity {

    @XmlElement
    private String clientName;

    @XmlElement
    private String caseNumber;

    @XmlElement
    private String mobileNumber;

    @XmlElement
    @XmlSchemaType(name = "judgementDate", type = Date.class)
    private Date judgementDate;

    @XmlElement
    @XmlSchemaType(name = "createdDate", type = Date.class)
    private Date createdDate;


    public ClientEntity() {
    }

    public ClientEntity(String clientName, String caseNumber, String mobileNumber, Date judgementDate, Date createdDate) {
        this.clientName = clientName;
        this.caseNumber = caseNumber;
        this.mobileNumber = mobileNumber;
        this.judgementDate = judgementDate;
        this.createdDate = createdDate;
    }


    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public String getJudgementDate() {
       if(null != judgementDate){
            return new LocalDate(judgementDate.getTime()).toString();
       }
        return null;
    }

    public String getCreatedDate() {
        return new LocalDate(createdDate.getTime()).toString();
    }
}
